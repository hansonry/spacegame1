settings = NewSettings()
if family == "windows" then
   settings.cc.defines:Add(" _CRT_SECURE_NO_WARNINGS")
   settings.cc.includes:Add("lib/SDL2-2.0.10/include")
   settings.cc.includes:Add("lib/glew-2.1.0/include")
   settings.link.libpath:Add("lib/SDL2-2.0.10/lib/x64")
   settings.link.libpath:Add("lib/glew-2.1.0/lib/Release/x64")
   settings.link.flags:Add("-Xlinker /subsystem:windows") -- clang Microsoft Linker   
   settings.link.libs:Add("SDL2main")
   settings.link.libs:Add("opengl32")
   settings.link.libs:Add("glew32")
   settings.link.libs:Add("SDL2")
else
   settings.cc.flags:Add("`sdl2-config --cflags`")
   settings.link.flags:Add("`sdl2-config --libs`")
   settings.link.libs:Add("GLEW")
   settings.link.libs:Add("GL")
end

settings.cc.includes:Add("lib/cglm/include")
settings.cc.includes:Add("lib/ryanxml/src")


source = Collect("src/*.c", "lib/ryanxml/src/*.c")
objects = Compile(settings, source);
exe = Link(settings, "spacegame", objects)

if family == "windows" then
   AddDependency(exe, CopyFile("SDL2.dll",   "lib/SDL2-2.0.10/lib/x64/SDL2.dll"))
   AddDependency(exe, CopyFile("glew32.dll", "lib/glew-2.1.0/bin/Release/x64/glew32.dll"))
end
