#version 330 core
out vec4 FragColor;
  
in vec4 vertexColor; // the input variable from the vertex shader (same name and same type)  
in vec3 Normal;
in vec3 FragPos;

uniform vec3 lightPos;

void main()
{
   vec3 norm;
   vec3 lightDir;
   vec3 diffuse;
   vec3 ambient;
   vec3 lightColor;
   float diff;
   float ambientStrength = 0.1;

   lightColor = vec3(1.0, 1.0, 1.0);

   ambient = ambientStrength * lightColor;

   norm = normalize(Normal);
   lightDir = normalize(lightPos - FragPos);

   diff = max(dot(norm, lightDir), 0.0);
   diffuse = diff * lightColor;

   FragColor = vec4((ambient + diffuse) * vec3(vertexColor), vertexColor.w);
}

