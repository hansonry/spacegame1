#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GL/glew.h>
#include <SDL_opengl.h>
#include "ryanxml.h"
#include "COLLADALoader.h"
#include "VertexBufferBuilder.h"

static float * COLLADALoader_parsefloats(const char * string, int count)
{
   float * base;
   const char * s;
   char * n;
   int i;
   s = string;
   base = malloc(sizeof(float) * count);
   for(i = 0; i < count; i++)
   {
      base[i] = (float)strtod(s, &n);
      s = n;
   }
   return base;
}

static unsigned int * COLLADALoader_ParseInts(const char * string, int count)
{
   unsigned int * base;
   const char * s;
   char * n;
   int i;
   s = string;
   base = malloc(sizeof(unsigned int) * count);
   for(i = 0; i < count; i++)
   {
      base[i] = (unsigned int)strtol(s, &n, 10);
      s = n;
   }
   return base;
}

static struct ryanxml_node * COLLADALoader_findid(struct ryanxml_node * start, 
                                                  const char * id)
{
   struct ryanxml_node * n;
   const char * value;
   
   n = start;
   
   
   while(n != NULL) // This should never be false, but I wanted to put something
   {
      value = ryanxml_findvalue(n, "@id");
      if(value != NULL && strcmp(value, id) == 0)
      {
         return n;
      }
      else if(n->childElement != NULL)
      {
         n = n->childElement;
      }
      else if(n->next != NULL)
      {
         n = n->next;
      }
      else
      {
         while(n != NULL && n->next == NULL && n != start)
         {
            n = n->parent;
         }

         if(n == NULL || n->next == NULL)
         {
            return NULL;
         }
         else
         {
            n = n->next;
         }

      }
   }
   return NULL;
}

static int COLLADALoader_StringToInt(const char * string)
{
   if(string == NULL)
   {
      return -1;
   }
   return strtol(string, NULL, 10);
}

struct sourceInfo
{
   float * base;
   int arraySize;
   int componentSize;
   int componentCount;
   int offset;
   int order[3];
};

static int COLLADALoader_FindAndParseSource(struct ryanxml_node * geom, 
                                            const char * sourceId,
                                            struct sourceInfo * sourceInfo)
{
   struct ryanxml_node * source;
   struct ryanxml_node * accessor;
   struct ryanxml_node * floatarray;
   struct ryanxml_node * param;
   const char * dataSource;
   const char * textFloatArray;
   int i;

   source = COLLADALoader_findid(geom, &sourceId[1]);
   if(source == NULL)
   {
      fprintf(stderr, "[COLLADALoader] Failed to find source: %s\n", sourceId);
      sourceInfo->base = NULL;
      return 0;
   }

   accessor = ryanxml_find(source, "technique_common/accessor");
   if(accessor == NULL)
   {
      fprintf(stderr, "[COLLADALoader] Failed to find accessor for source: %s\n", sourceId);
      sourceInfo->base = NULL;
      return 0;
   }

   sourceInfo->componentSize = COLLADALoader_StringToInt(ryanxml_findvalue(accessor, "@stride"));
   sourceInfo->componentCount = COLLADALoader_StringToInt(ryanxml_findvalue(accessor, "@count"));
   dataSource = ryanxml_findvalue(accessor, "@source");

   floatarray = COLLADALoader_findid(source, &dataSource[1]);

   sourceInfo->arraySize = COLLADALoader_StringToInt(ryanxml_findvalue(floatarray, "@count"));

   if(sourceInfo->arraySize < 0)
   {
      fprintf(stderr, "[COLLADALoader] Failed to get float_array count for source: %s\n", sourceId);
      sourceInfo->base = NULL;
      return 0;
   }

   textFloatArray = ryanxml_findvalue(floatarray, "");
   sourceInfo->base = COLLADALoader_parsefloats(textFloatArray, sourceInfo->arraySize);

   param = accessor->childElement;

   for(i = 0; i < sourceInfo->componentSize; i++)
   {
      const char * name;
      if(param != NULL)
      {
         name = ryanxml_findvalue(param, "@name");
         switch(name[0])
         {
         case 'X':
         case 'S':
            sourceInfo->order[i] = 0;
            break;
         case 'Y':
         case 'T':
            sourceInfo->order[i] = 1;
            break;
         case 'Z':
            sourceInfo->order[i] = 2;
            break;
         default:
            sourceInfo->order[i] = -1;
            break;
         }
         param = param->next;
      }
      else
      {
         sourceInfo->order[i] = -1;
      }
   }

   return 1;
}

static void COLLADALoader_ParseGeometry(struct ryanxml_node * geom, struct colladaloader_model * model)
{
   const char * name;
   const char * source;
   const char * vertex_source;
   const char * textTriangleIndexList;
   struct ryanxml_node * triangles;
   struct ryanxml_node * triangles_vertex_input;
   struct ryanxml_node * vertices;
   struct ryanxml_node * vertices_position_input;
   struct ryanxml_node * input;
   struct sourceInfo vertexSourceInfo;
   struct sourceInfo normalSourceInfo;
   struct sourceInfo textCoordSourceInfo;
   int vertexOffset, normalOffset, texCoordOffset;
   unsigned int * triangleIndexBase;
   int indexPerTriangle;
   
   
   int triangleDataCount;

   int i;

   struct vertexbufferbuilder * vbb;
   FILE *fh;

   name = ryanxml_findvalue(geom, "@name");
   printf("Geometry Name: %s\n", name);

   // Lets find the triangles and then work our way back

   triangles = ryanxml_find(geom, "mesh/triangles");
   if(triangles == NULL)
   {
      fprintf(stderr, "[COLLADALoader] Failed to find mesh/triangles xml node\n");
      return;
   }

   triangleDataCount = COLLADALoader_StringToInt(ryanxml_findvalue(triangles, "@count")) * 3;
   printf("Triangles has %d data\n", triangleDataCount);
   triangles_vertex_input = ryanxml_find(triangles, "input@semantic=VERTEX");
   vertexOffset = COLLADALoader_StringToInt(ryanxml_findvalue(triangles_vertex_input, "@offset"));

   source = ryanxml_findvalue(triangles_vertex_input, "@source");
   printf("Vertext Source: %s\n", source );
   vertices = COLLADALoader_findid(geom, &source[1]);
   if(vertices == NULL)
   {
      fprintf(stderr, "[COLLADALoader] Couldn't find ID: %s\n", source);
      return;
   }

   printf("Found: %s\n", vertices->name);

   vertices_position_input = ryanxml_find(vertices, "input@semantic=POSITION");

   vertex_source = ryanxml_findvalue(vertices_position_input, "@source"); 

   COLLADALoader_FindAndParseSource(geom, vertex_source, &vertexSourceInfo);

   input = ryanxml_find(triangles, "input@semantic=NORMAL");
   source = ryanxml_findvalue(input, "@source");
   COLLADALoader_FindAndParseSource(geom, source, &normalSourceInfo);
   normalOffset = COLLADALoader_StringToInt(ryanxml_findvalue(input, "@offset"));

   input = ryanxml_find(triangles, "input@semantic=TEXCOORD");
   source = ryanxml_findvalue(input, "@source");
   COLLADALoader_FindAndParseSource(geom, source, &textCoordSourceInfo);
   texCoordOffset = COLLADALoader_StringToInt(ryanxml_findvalue(input, "@offset"));


   textTriangleIndexList = ryanxml_findvalue(triangles, "p");

   indexPerTriangle = triangles->childElementCount - 1;
   triangleIndexBase = COLLADALoader_ParseInts(textTriangleIndexList, triangleDataCount * indexPerTriangle);

   // Load all the data into two buffers. One is the data buffer and
   // the other is the index buffer.

   vbb = VertexBufferBuilder_Create(indexPerTriangle, triangleIndexBase, triangleDataCount * indexPerTriangle);
   
   VertexBufferBuilder_SetBuffer(vbb, vertexOffset, vertexSourceInfo.base, vertexSourceInfo.arraySize, vertexSourceInfo.order);
      
   VertexBufferBuilder_SetBuffer(vbb, normalOffset, normalSourceInfo.base, normalSourceInfo.arraySize, normalSourceInfo.order);

   VertexBufferBuilder_Build(vbb);


   // Load stuff into gfx memeory

    
   glGenVertexArrays(1, &model->VAO);
   glBindVertexArray(model->VAO);
   
   glGenBuffers(1, &model->buffer);
   glGenBuffers(1, &model->elementBuffer);

   glBindBuffer(GL_ARRAY_BUFFER, model->buffer);
   glBufferData(GL_ARRAY_BUFFER, 
                VertexBufferBuilder_GetBufferSize(vbb) * sizeof(float), 
                VertexBufferBuilder_GetBuffer(vbb), 
                GL_STATIC_DRAW);
   
   
   glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
   glEnableVertexAttribArray(0);
   
   glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(sizeof(float) * 3));
   glEnableVertexAttribArray(1);

   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model->elementBuffer);
   glBufferData(GL_ELEMENT_ARRAY_BUFFER, 
                VertexBufferBuilder_GetElementSize(vbb) * sizeof(unsigned int),
                VertexBufferBuilder_GetElements(vbb), 
                GL_STATIC_DRAW);

   model->VAOSize = VertexBufferBuilder_GetElementSize(vbb);
   // Cleanup


   VertexBufferBuilder_Destory(vbb);


   free(vertexSourceInfo.base);
   free(normalSourceInfo.base);
   free(textCoordSourceInfo.base);
   free(triangleIndexBase);
}

int COLLADALoader_Load(const char * filename, struct colladaloader_model * models)
{
   struct ryanxml_node * root;
   struct ryanxml_node * lib_geom;
   struct ryanxml_node * geom;
   int count;

   count = 0;
   root = ryanxml_parsefile(filename, 0);
   lib_geom = ryanxml_find(root, "/COLLADA/library_geometries");
   geom = lib_geom->childElement;
   if(geom != NULL && 
      geom->type == e_rxnt_element && 
      strcmp(geom->name, "geometry") == 0)
   {
        COLLADALoader_ParseGeometry(geom, &models[count]);
        count ++;
   }

   ryanxml_deletenode(root);
   return count;;
}

