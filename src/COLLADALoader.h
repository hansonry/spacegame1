#ifndef __COLLADALOADER_H__
#define __COLLADALOADER_H__
#include <SDL_opengl.h>

struct colladaloader_model
{
   GLuint VAO;
   GLuint buffer;
   GLuint elementBuffer;
   unsigned int VAOSize;
};

int COLLADALoader_Load(const char * filename, struct colladaloader_model * models);

#endif // __COLLADALOADER_H__

