#ifndef __SHADERTOOLS_H__
#define __SHADERTOOLS_H__
#include "Constants.h"
#include <GL/glew.h>
#include <SDL_opengl.h>


enum result ShaderTools_LoadAndBuildShader(GLuint shaderProgramHandle,
                                           const char * vertexShaderFilename,
                                           const char * geometryShaderFilename,
                                           const char * fragmentShaderFilename);


enum result ShaderTools_LoadShader(GLuint shaderHandle, 
                                   const char * shaderFilename);
enum result ShaderTools_CompileShader(GLuint shaderHandle);
enum result ShaderTools_LinkProgram(GLuint programHandle);

#endif // __SHADERTOOLS_H__
