#include <stdio.h>
#include <GL/glew.h>
#include <SDL.h>
#include <SDL_opengl.h>
#include "Constants.h"
#include "ShaderTools.h"
#include "cglm/cglm.h"
#include "COLLADALoader.h"


enum camerakeys
{
   e_ck_forward,
   e_ck_backward,
   e_ck_length
};


const SDL_Scancode keyScanCodes[e_ck_length] =
{
   SDL_SCANCODE_W,
   SDL_SCANCODE_S
};

struct camera
{
   mat4 viewMatrix;
   vec3 position;
   bool keyStates[e_ck_length];
   float distance;
   float horiAngle;
   float virtAngle;
};

static void Camera_UpdateMatrix(struct camera * camera)
{
   mat4 mat;
   glm_mat4_identity(mat);
   glm_translate_z(mat, -camera->distance);
   glm_rotate_x(mat, camera->virtAngle, mat);
   glm_rotate_y(mat, camera->horiAngle, mat);
   glm_translate(mat, camera->position);
            
   glm_mat4_copy(mat, camera->viewMatrix);
}

void Camera_Init(struct camera * camera)
{
   int i;
   camera->position[0] = 0;
   camera->position[1] = 0;
   camera->position[2] = 0;
   camera->distance = 10;
   camera->horiAngle = 0;
   camera->virtAngle = 0;
   
   for(i = 0; i < e_ck_length; i++)
   {
      camera->keyStates[i] = false;
   }
   
   Camera_UpdateMatrix(camera);
}

void Camera_SDLEvent(struct camera * camera, SDL_Event * event)
{
   int i;
   int keyIndex;
   bool state;
   if(event->type == SDL_KEYDOWN || 
      event->type == SDL_KEYUP)
   {
      keyIndex = e_ck_length;
      for(i = 0; i < e_ck_length; i++)
      {
         if(event->key.keysym.scancode == keyScanCodes[i])
         {
            keyIndex = i;
            break;
         }
      }
      
      if(event->type == SDL_KEYDOWN)
      {
         state = true;
      }
      else
      {
         state = false;
      }
      
      if(keyIndex < e_ck_length)
      {
         camera->keyStates[keyIndex] = state;
      }
   }
   else if(event->type == SDL_MOUSEMOTION)
   {
      if((event->motion.state & SDL_BUTTON_RMASK) == SDL_BUTTON_RMASK)
      {
         camera->horiAngle -= 0.02 * event->motion.xrel;
         camera->virtAngle -= 0.02 * event->motion.yrel;
         
         // Clamp the virtical camera
         if(camera->virtAngle > glm_rad(90))
         {
            camera->virtAngle = glm_rad(90);
         }
         else if(camera->virtAngle < glm_rad(-90))
         {
            camera->virtAngle = glm_rad(-90);
         }
      }
   }
   else if (event->type == SDL_MOUSEWHEEL)
   {
      camera->distance += 0.1 * event->wheel.y;
   }
}

void Camera_Update(struct camera * camera, float seconds)
{
   
   Camera_UpdateMatrix(camera);
}

void Camera_SetPosition(struct camera * camera, float x, float y, float z)
{
   camera->position[0] = -x;
   camera->position[1] = -y;
   camera->position[2] = -z;
}

void Camera_GetMatrix(struct camera * camera, mat4 viewMatrix)
{
   glm_mat4_copy(camera->viewMatrix, viewMatrix);
}



float triangleData[] = 
{
   -0.5f, -0.5f, 0.0f,
    0.5f, -0.5f, 0.0f,
    0.0f,  0.5f, 0.0f
};

int main(int argc, char * args[])
{
   SDL_Window * window;
   int running;
   
   Uint32 prevTimer;
   Uint32 timer;
   float seconds;
   
   GLuint shader;
   GLuint VBO;
   GLuint VAO;
   struct colladaloader_model model;
   
   GLint uniformModel, uniformView, uniformProjection, uniformLightPos;
   
   mat4 projectionMatrix;
   mat4 viewMatrix;
   mat4 modelMatrix;

   float x, roll;
   
   struct camera cam;
   
   SDL_Init(SDL_INIT_EVERYTHING);
   
   window = SDL_CreateWindow("Space Game", 
                             SDL_WINDOWPOS_CENTERED, 
                             SDL_WINDOWPOS_CENTERED, 
                             1024, 768, 
                             SDL_WINDOW_SHOWN | 
                             SDL_WINDOW_OPENGL |
                             SDL_WINDOW_RESIZABLE);


   SDL_GL_SetAttribute(SDL_GL_RED_SIZE,     8);
   SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,   8);
   SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,    8);
   SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,   8);
   SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
   SDL_GL_CreateContext(window);
   
   glewExperimental = GL_TRUE;
   glewInit();
   
   glEnable(GL_CULL_FACE);
   glCullFace(GL_BACK);
   glFrontFace(GL_CCW);

   glEnable(GL_DEPTH_TEST);
   glDepthFunc(GL_LESS);
   
   // === Init Stuff === //
   // Load Shaders
   printf("Building Shaders...\n");
   shader = glCreateProgram();
   ShaderTools_LoadAndBuildShader(shader, "shaders/shaded.vert", NULL, "shaders/shaded.frag");
   printf("Building Shaders Completed\n");
   printf("Loading Models...\n");
   COLLADALoader_Load("assets/Freighter1.dae", &model);
   printf("Loading Models Completed\n");
   
   // Setup Data for triangle
   glGenVertexArrays(1, &VAO);
   glBindVertexArray(VAO);
   
   glGenBuffers(1, &VBO);
   glBindBuffer(GL_ARRAY_BUFFER, VBO);
   glBufferData(GL_ARRAY_BUFFER, sizeof(triangleData), triangleData, GL_STATIC_DRAW);
   
   
   // Configure how data is linked with shader
   glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
   glEnableVertexAttribArray(0);
   
   
   // Setup Matrix things
   uniformModel      = glGetUniformLocation(shader, "model");
   uniformView       = glGetUniformLocation(shader, "view");
   uniformProjection = glGetUniformLocation(shader, "projection");
   uniformLightPos   = glGetUniformLocation(shader, "lightPos");
   
   //printf("Uniforms: %d, %d, %d\n", uniformModel, uniformView, uniformProjection);
   
   glm_perspective( glm_rad(60), 1024 / 768.0, 1, 100, projectionMatrix);
   //glm_mat4_identity(projectionMatrix); 
   
   glm_mat4_identity(modelMatrix);
      
   // Setup Shader to draw stuff
   glUseProgram(shader);
   glBindVertexArray(VAO);
   
   // Do uniform things
   glUniformMatrix4fv(uniformProjection, 1, GL_FALSE, projectionMatrix[0]);
   glUniformMatrix4fv(uniformModel,      1, GL_FALSE, modelMatrix[0]);
   glUniform3f(uniformLightPos,          0, 0, 10);
   
   // Setup Camera
   Camera_Init(&cam);
   
   x = 0;
   roll = 0;
   prevTimer = SDL_GetTicks();
   running = 1;
   while(running)
   {
      // === Input === //
      SDL_Event event;
      while( SDL_PollEvent(&event) )
      {
         Camera_SDLEvent(&cam, &event);
         switch(event.type)
         {
         case SDL_QUIT:
            running = 0;
            break;
         case SDL_WINDOWEVENT:
            if(event.window.event == SDL_WINDOWEVENT_RESIZED)
            {
               glViewport(0, 0, event.window.data1, event.window.data2);
               glm_perspective(glm_rad(60), event.window.data1 / (float)event.window.data2, 1, 100, projectionMatrix);
               glUniformMatrix4fv(uniformProjection, 1, GL_FALSE, projectionMatrix[0]);
            }
            break;
         }
         
      }
      
      // === Updating === //
      timer = SDL_GetTicks();
      seconds = (timer - prevTimer) / 1000.0f;
      prevTimer = timer;
      
      x += seconds * .5;
      roll += seconds * glm_rad(9);
      Camera_SetPosition(&cam, x, 0, 0);
      Camera_Update(&cam, seconds);
      
      // === Graphics Rendering === //
      //glClearColor ( 0.2, 0.2, 0.4, 1.0 );
      glClearColor ( 0.0, 0.0, 0.0, 1.0 );
      glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
      
      Camera_GetMatrix(&cam, viewMatrix);
      glUniformMatrix4fv(uniformView, 1, GL_FALSE, viewMatrix[0]);

      glBindVertexArray(VAO);
      glDrawArrays(GL_TRIANGLES, 0, 3);

      glBindVertexArray(model.VAO);

      glm_mat4_identity(modelMatrix);
      glm_rotate_x(modelMatrix, glm_rad(-90), modelMatrix);
      glUniformMatrix4fv(uniformModel,      1, GL_FALSE, modelMatrix[0]);

      glDrawElements(GL_TRIANGLES, model.VAOSize, GL_UNSIGNED_INT, (void*)0);

      glBindVertexArray(model.VAO);

      glm_mat4_identity(modelMatrix);
      glm_rotate_x(modelMatrix, glm_rad(-90) + roll, modelMatrix);
      glm_translate_x(modelMatrix, x);
      glUniformMatrix4fv(uniformModel,      1, GL_FALSE, modelMatrix[0]);

      glDrawElements(GL_TRIANGLES, model.VAOSize, GL_UNSIGNED_INT, (void*)0);

      
      SDL_GL_SwapWindow(window);
   }
   
    glDeleteProgram(shader);
   
   SDL_DestroyWindow(window);
   SDL_Quit();
   printf("End\n");
   return 0;
}
