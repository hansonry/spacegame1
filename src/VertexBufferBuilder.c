#include <stdlib.h>
#include <string.h>
#include "VertexBufferBuilder.h"


struct vbb_buffer
{
   int offset;
   float * base;
   unsigned int size;
   int order[3];
   int stride;
};

struct vertexbufferbuilder
{
   unsigned int element_stride;
   const unsigned int * element_base;
   unsigned int element_size;
   struct vbb_buffer * base;
   unsigned int * new_element_base;
   unsigned int new_element_size;
   unsigned int new_element_count;
   float * new_buffer_base;
   unsigned int new_buffer_size;
   unsigned int new_buffer_count;
   int vertex_size;
};

#define GROWBY 32


/// @param[in] base_size The size of the base array in elements
struct vertexbufferbuilder *  VertexBufferBuilder_Create(unsigned int element_stride, const unsigned int * base, unsigned int base_size)
{
   struct vertexbufferbuilder * vbb;
   int i;
   vbb = malloc(sizeof(struct vertexbufferbuilder));
   vbb->element_stride = element_stride;
   vbb->element_base = base;
   vbb->element_size = base_size;

   vbb->base = malloc(sizeof(struct vbb_buffer) * element_stride);

   for(i = 0; i < element_stride; i++)
   {
      vbb->base[i].offset = -1;
   }

   vbb->new_element_base = NULL;
   vbb->new_element_count = 0;
   vbb->new_element_size = 0;
   vbb->new_buffer_base = NULL;
   vbb->new_buffer_count = 0;
   vbb->new_buffer_size = 0;
  
   return vbb;
}

void VertexBufferBuilder_Destory(struct vertexbufferbuilder * vbb)
{
   if(vbb->new_element_base != NULL)
   {
      free(vbb->new_element_base);
   }
   if(vbb->new_buffer_base != NULL)
   {
      free(vbb->new_buffer_base);
   }

   free(vbb->base);
   free(vbb);
}

void VertexBufferBuilder_SetBuffer(struct vertexbufferbuilder * vbb, int offset, float * base, unsigned int base_size, int order[3]) 
{
   int i;
   vbb->base[offset].stride = 0;
   if(order != NULL)
   {
      for(i = 0; i < 3; i++)
      {
         vbb->base[offset].order[i] = order[i];
         if(order[i] >= 0)
         {
            vbb->base[offset].stride ++;
         }
      }
   }

   if(base == NULL)
   {
      vbb->base[offset].offset = -1;
   }
   else
   {
      vbb->base[offset].offset = offset;
   }
   vbb->base[offset].base = base;
   vbb->base[offset].size = base_size;


}

static void VertexBufferBuild_AddNewElement(struct vertexbufferbuilder * vbb,
                                            unsigned int elementIndex)
{
   if(vbb->new_element_count >=  vbb->new_element_size)
   {
      unsigned int newSize;
      newSize = vbb->new_element_count + GROWBY;
      vbb->new_element_base = realloc(vbb->new_element_base, sizeof(unsigned int) * newSize);
      vbb->new_element_size = newSize;
   }

   vbb->new_element_base[vbb->new_element_count] = elementIndex;
   vbb->new_element_count ++;
}

static void VertexBufferBuilder_AddNewVertex(struct vertexbufferbuilder * vbb,
                                             float * vertex,
                                             int size)
{
   if(vbb->new_buffer_count >= vbb->new_buffer_size)
   {
      unsigned int newSize;
      newSize = vbb->new_element_count + GROWBY;
      vbb->new_buffer_base = realloc(vbb->new_buffer_base, sizeof(float) * size * newSize);
      vbb->new_buffer_size = newSize;
   }

   memcpy(&vbb->new_buffer_base[vbb->new_buffer_count * size], vertex, sizeof(float) * size);
   vbb->new_buffer_count ++;


}


static void VertexBufferBuilder_AddVertex(struct vertexbufferbuilder * vbb, 
                                          float * vertex, 
                                          int vertexSizeInFloats)
{
   int i;
   int found;
   unsigned int elementIndex;
   found = 0;
   elementIndex = 0;
   for(i = 0; i < vbb->new_buffer_count; i ++)
   {
      if(memcmp(vertex, 
                &vbb->new_buffer_base[i * vertexSizeInFloats], 
                vertexSizeInFloats * sizeof(float)) == 0)
      {
         found = 1;
         break;
      }
      elementIndex ++;
   }

   if(found == 0)
   {
      VertexBufferBuilder_AddNewVertex(vbb, vertex, vertexSizeInFloats);
   }

   VertexBufferBuild_AddNewElement(vbb, elementIndex);
}


void VertexBufferBuilder_Build(struct vertexbufferbuilder * vbb)
{
   float * vertex;
   int vertexSizeInFloats;
   int vertexSizeInComp;
   unsigned int index, bufferIndex;
   const unsigned int *ele;
   float * v;
   int i, k, l;

   vertexSizeInFloats = 0;
   vertexSizeInComp = 0;
   for(i = 0; i < vbb->element_stride; i++)
   {
      if(vbb->base[i].offset >= 0)
      {
         vertexSizeInComp ++;
         for(k = 0; k < 3; k++)
         {
            if(vbb->base[i].order[k] >= 0)
            {
               vertexSizeInFloats ++;
            }
         }
      }
   }

   vertex = malloc(sizeof(float) * vertexSizeInFloats);
   vbb->new_element_count = 0;
   vbb->new_element_size = 0;

   ele = vbb->element_base;

   for(i = 0; i < vbb->element_size; i+= vbb->element_stride)
   {
      //index = i * vertexSizeInComp;
      v = vertex;
      for(k = 0; k < vbb->element_stride; k++)
      {
         if(vbb->base[k].offset >= 0)
         {
            //bufferIndex = vbb->base[k].stride * (index + vbb->base[k].offset);
            for(l = 0; l < 3; l++)
            {
               if(vbb->base[k].order[l] >= 0)
               {
                  v[vbb->base[k].order[l]] = 
                     vbb->base[k].base[ele[vbb->base[k].offset] * vbb->base[k].stride + l];
               }
            }
            v += vbb->base[k].stride;
         }
      }
      VertexBufferBuilder_AddVertex(vbb, vertex, vertexSizeInFloats);
      ele += vbb->element_stride;
   }

   vbb->vertex_size = vertexSizeInFloats;
   free(vertex);
}

const float * VertexBufferBuilder_GetBuffer(struct vertexbufferbuilder * vbb)
{
   return vbb->new_buffer_base;
}

unsigned int VertexBufferBuilder_GetBufferSize(struct vertexbufferbuilder * vbb)
{
   return vbb->new_buffer_count * vbb->vertex_size;
}

const unsigned int * VertexBufferBuilder_GetElements(struct vertexbufferbuilder * vbb)
{
   return vbb->new_element_base;
}

unsigned int VertexBufferBuilder_GetElementSize(struct vertexbufferbuilder * vbb)
{
   return vbb->new_element_count;
}


