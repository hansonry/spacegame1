#include "simulation.h"




void integrate_vec3s32(struct vec3s32 * p, 
                       const struct vec3s32 * v, 
                       int milliseconds)
{

   p->x = p->x + (v->x * milliseconds / 1000);

   p->y = p->y + (v->y * milliseconds / 1000);

   p->z = p->z + (v->z * milliseconds / 1000);
}

