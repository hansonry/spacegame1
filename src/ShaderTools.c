#include <stdio.h>
#include <stdlib.h>
#include "ShaderTools.h"


enum result ShaderTools_LoadShader(GLuint shaderHandle, 
                                   const char * shaderFilename)
{
   GLint fileSize;
   FILE * fileHandle;
   GLchar * fileBuffer;
   
   fileHandle = fopen(shaderFilename, "rb");
   if(fileHandle == NULL)
   {
      fprintf(stderr, "Error opening Shader File \"%s\"\n", shaderFilename);
      return failure;
   }
   fseek(fileHandle, 0, SEEK_END);
   fileSize = (GLint)ftell(fileHandle);
   
   fseek(fileHandle, 0, SEEK_SET);
   fileBuffer = malloc(fileSize);
   fread(fileBuffer, 1, fileSize, fileHandle);
   fclose(fileHandle);
   
   
   glShaderSource(shaderHandle, 1, (const GLchar **)&fileBuffer, &fileSize);
   free(fileBuffer);
   
   return success;
}

enum result ShaderTools_CompileShader(GLuint shaderHandle)
{
   GLint compileSuccess;
   
   glCompileShader(shaderHandle);
   glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compileSuccess);
   if(compileSuccess == GL_FALSE)
   {
      GLchar * log;
      GLint logSize = 0;
      glGetShaderiv(shaderHandle, GL_INFO_LOG_LENGTH, &logSize);
      log = malloc(logSize);
      glGetShaderInfoLog(shaderHandle, logSize, &logSize, log);
      fprintf(stderr, "Shader Compile Error: %s\n", log);
      free(log);
      return failure;
   }
   
   return success;
}


enum result ShaderTools_LinkProgram(GLuint programHandle)
{
   GLint linkSuccess = GL_FALSE;
   
   glLinkProgram(programHandle);
   glGetProgramiv(programHandle, GL_LINK_STATUS, &linkSuccess);
   if(linkSuccess == GL_FALSE)
   {
      GLchar * log;
      GLint logSize = 0;
      glGetProgramiv(programHandle, GL_INFO_LOG_LENGTH, &logSize);
      log = malloc(logSize);
      glGetProgramInfoLog(programHandle, logSize, &logSize, log);
      fprintf(stderr, "Shader Link Error: %s\n", log);
      free(log);
      return failure;
   }
   return success;
}

static enum result ShaderTools_LoadAndCompileShader(GLuint shaderHandle, 
                                                    const char * shaderFilename)
{
   if(ShaderTools_LoadShader(shaderHandle, shaderFilename) == success &&
      ShaderTools_CompileShader(shaderHandle) == success)
   {
      return success;
   }
   return failure;
}

enum result ShaderTools_LoadAndBuildShader(GLuint shaderProgramHandle,
                                           const char * vertexShaderFilename,
                                           const char * geometryShaderFilename,
                                           const char * fragmentShaderFilename)
{
   GLuint vertexShaderHandle;
   GLuint geometryShaderHandle;
   GLuint fragmentShaderHandle;
   
   bool vertexShaderAttached = false;
   bool geometryShaderAttached = false;
   bool fragmentShaderAttached = false;
   
   
   enum result result = success;
   
   if(vertexShaderFilename != NULL && result == success)
   {
      vertexShaderHandle = glCreateShader(GL_VERTEX_SHADER);
      if(ShaderTools_LoadAndCompileShader(vertexShaderHandle, vertexShaderFilename) == success)
      {
         glAttachShader(shaderProgramHandle, vertexShaderHandle);
         vertexShaderAttached = true;
      }
      else
      {
         glDeleteShader(vertexShaderHandle);
         result = failure;
      }
   }
   if(geometryShaderFilename != NULL && result == success)
   {
      geometryShaderHandle = glCreateShader( GL_GEOMETRY_SHADER);
      if(ShaderTools_LoadAndCompileShader(geometryShaderHandle, geometryShaderFilename) == success)
      {
         glAttachShader(shaderProgramHandle, geometryShaderHandle);
         geometryShaderAttached = true;
      }
      else
      {
         glDeleteShader(geometryShaderHandle);
         result = failure;
      }
   }
   if(fragmentShaderFilename != NULL && result == success)
   {
      fragmentShaderHandle = glCreateShader( GL_FRAGMENT_SHADER);
      if(ShaderTools_LoadAndCompileShader(fragmentShaderHandle, fragmentShaderFilename) == success)
      {
         glAttachShader(shaderProgramHandle, fragmentShaderHandle);
         fragmentShaderAttached = true;
      }
      else
      {
         glDeleteShader(fragmentShaderHandle);
         result = failure;
      }
   }
   
   
   if(result == success)
   {
      result = ShaderTools_LinkProgram(shaderProgramHandle);
   }
   
   // Cleanup Shaders
   if(vertexShaderAttached == true)
   {
      glDetachShader(shaderProgramHandle, vertexShaderHandle);
      glDeleteShader(vertexShaderHandle);
   }
   if(geometryShaderAttached == true)
   {
      glDetachShader(shaderProgramHandle, geometryShaderHandle);
      glDeleteShader(geometryShaderHandle);
   }
   if(fragmentShaderAttached == true)
   {
      glDetachShader(shaderProgramHandle, fragmentShaderHandle);
      glDeleteShader(fragmentShaderHandle);
   }
   
   return result;
}
