#ifndef __VERTEXBUFFERBUILDER_H__
#define __VERTEXBUFFERBUILDER_H__

struct vertexbufferbuilder;


/// @param[in] base_size The size of the base array in elements
struct vertexbufferbuilder *  VertexBufferBuilder_Create(unsigned int element_stride, const unsigned int * base, unsigned int base_size);

void VertexBufferBuilder_Destory(struct vertexbufferbuilder * vbb);

void VertexBufferBuilder_SetBuffer(struct vertexbufferbuilder * vbb, int offset, float * base, unsigned int base_size, int order[3]); 



void VertexBufferBuilder_Build(struct vertexbufferbuilder * vbb);

const float * VertexBufferBuilder_GetBuffer(struct vertexbufferbuilder * vbb);

unsigned int VertexBufferBuilder_GetBufferSize(struct vertexbufferbuilder * vbb);

const unsigned int * VertexBufferBuilder_GetElements(struct vertexbufferbuilder * vbb);

unsigned int VertexBufferBuilder_GetElementSize(struct vertexbufferbuilder * vbb);



#endif // __VERTEXBUFFERBUILDER_H__

