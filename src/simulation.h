#ifndef __SIMULATION_H__
#define __SIMULATION_H__
#include <stdint.h>

struct vec3s32
{
   int32_t x;
   int32_t y;
   int32_t z;
};


void integrate_vec3s32(struct vec3s32 * p, 
                       const struct vec3s32 * v, 
                       int milliseconds);

#endif // __SIMULATION_H__

